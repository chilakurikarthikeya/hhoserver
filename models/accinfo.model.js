const mongoose = require('mongoose');

var AccInfo_Schema = new mongoose.Schema({

    post_by: {
        type: String,
        require: 'cant be empty'
    },
    date: {
        type: String,
        require: 'cant be empty'
    },
    batch: {
        type: String,
        require: 'cant be empty'
    },
    branch: {
        type: String,
        require: 'cant be empty'
    },
    class: {
        type: String,
            require: 'cant be empty'
    },
    purpose: {
        type: String,
        require: 'cant be empty'
    },
    collected_amount: {
        type: String,
        require: 'cant be empty'
    },
    total_money: {
        type: String
    }


});

AccInfo_Schema.pre('save', function(next) {
    next();
});
mongoose.model('Acc_Info', AccInfo_Schema);


//account balance schema
var AccBalance_Schema = new mongoose.Schema({

    Total_Money: {
        type: Number,
        require: 'cant be empty'
    },

});

AccBalance_Schema.pre('save', function(next) {
    next();
});
mongoose.model('AccBalance', AccBalance_Schema);


// account sheet schema

//account balance schema
var AccSheet_Schema = new mongoose.Schema({

    update_by: {
        type: String,
        require: 'cant be empty'
    },
    amount: {
        type: Number,
        require: 'cant be empty'
    },
    purpose: {
        type: String,
        require: 'cant be empty'
    },
    total_amount: {
        type: Number,
        require: 'cant be empty'
    },

});

AccSheet_Schema.pre('save', function(next) {
    next();
});
mongoose.model('AccSheet', AccSheet_Schema);