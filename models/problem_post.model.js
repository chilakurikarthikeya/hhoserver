const mongoose = require('mongoose');

var Problem_PostSchema = new mongoose.Schema({

    postby: {
        type: String,
        require: 'cant be empty'
    },
    Victim_Name: {
        type: String,
        require: 'cant be empty'
    },
    id_no: {
        type: String,
        require: 'cant be empty'
    },
    email: {
        type: String,
        require: 'cant be empty'
    },
    ph_no: {
        type: String,
        require: 'cant be empty'
    },
    branch: {
        type: String,
        require: 'cant be empty'
    },
    class: {
        type: String,
            require: 'cant be empty'
    },
    problem: {
        type: String,
        require: 'cant be empty'
    },
    problem_description: {
        type: String,
        require: 'cant be empty'
    },
    required_amount: {
        type: Number,
        require: 'cant be empty'
    },
    doc_path: {
        type: String,
        require: 'cant be empty'
    },
    status: {
        type: String,
        require: 'cant be empty'
    }

});

Problem_PostSchema.pre('save', function(next) {
    next();
});
mongoose.model('Posted_Problems', Problem_PostSchema);