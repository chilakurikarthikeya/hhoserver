const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

JWT_SECRET = 'log2';
JWT_EXP = '1d'

var userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: 'Full name can\'t be empty'
    },
    id: {
        type: String,
        required: 'id can\'t be empty',
        unique: true
    },

    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    class: {
        type: String,
            required: 'class can\'t be empty'

    },
    branch: {
        type: String,
        required: 'branch can\'t be empty'

    },
    ph_no: {
        type: String,
        required: 'id can\'t be empty'

    },
    designation: {
        type: String,
        required: 'id can\'t be empty'

    },
    moto_of_joining: {
        type: String,
        required: 'id can\'t be empty'

    },
    date_of_joining: {
        type: String,
        required: 'id can\'t be empty'

    },
    password: {
        type: String,
        required: 'Password can\'t be empty',
        minlength: [4, 'Password must be atleast 4 character long']
    },
    otp: {
        type: Boolean
    },
    admin_verify: {
        type: Boolean
    },
    saltSecret: String
});



// Custom validation for email
userSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

//events
userSchema.pre('save', function(next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});



// Methods
userSchema.methods.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.methods.generateJwt = function() {
    return jwt.sign({ _id: this._id },
        JWT_SECRET, {
            expiresIn: JWT_EXP
        });
}


mongoose.model('Rep', userSchema);




// students Schema

var studentSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: 'Full name can\'t be empty'
    },
    id: {
        type: String,
        required: 'id can\'t be empty',
        unique: true
    },

    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },
    class: {
        type: String,
            required: 'class can\'t be empty'

    },
    branch: {
        type: String,
        required: 'branch can\'t be empty'

    },
    ph_no: {
        type: String,
        required: 'id can\'t be empty'

    },
    password: {
        type: String,
        required: 'Password can\'t be empty',
        minlength: [4, 'Password must be atleast 4 character long']
    },
    otp: {
        type: Boolean
    },
    saltSecret: String
});

// Custom validation for email
studentSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

//events
studentSchema.pre('save', function(next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});

// Methods
studentSchema.methods.verifyPassword = function(password) {
    // console.log(bcrypt.compareSync(password, this.password));
    return bcrypt.compareSync(password, this.password);
};

studentSchema.methods.generateJwt = function() {
    console.log(this._id);
    return jwt.sign({ _id: this._id },
        JWT_SECRET, {
            expiresIn: JWT_EXP
        });
}


mongoose.model('Student', studentSchema);



// others schema

var othersSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: 'Full name can\'t be empty'
    },
    id: {
        type: String,
        required: 'id can\'t be empty',
        unique: true
    },

    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },

    ph_no: {
        type: String,
        required: 'id can\'t be empty'

    },
    password: {
        type: String,
        required: 'Password can\'t be empty',
        minlength: [4, 'Password must be atleast 4 character long']
    },
    otp: {
        type: Boolean
    },
    saltSecret: String
});

// Custom validation for email
othersSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

//events
othersSchema.pre('save', function(next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.saltSecret = salt;
            next();
        });
    });
});

// Methods
othersSchema.methods.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

othersSchema.methods.generateJwt = function() {
    return jwt.sign({ _id: this._id },
        JWT_SECRET, {
            expiresIn: JWT_EXP
        });
}


mongoose.model('Others', othersSchema);