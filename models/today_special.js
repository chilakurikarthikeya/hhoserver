const mongoose = require('mongoose');

var TodaySpecial_Schema = new mongoose.Schema({

    name: {
        type: String,
        require: 'cant be empty'
    },
    date: {
        type: String,
        require: 'cant be empty'
    },
    description: {
        type: String,
        require: 'cant be empty'
    }



});

TodaySpecial_Schema.pre('save', function(next) {
    next();
});
mongoose.model('TodaySpecial', TodaySpecial_Schema);