const mongoose = require('mongoose');

var AchieveSchema = new mongoose.Schema({
    imageUrl: {
        type: String,
        require: 'cant be empty'
    },
    name: {
        type: String,
        require: 'cant be empty'
    },
    problem: {
        type: String,
        require: 'cant be empty'
    },
    amount_donated: {
        type: String,
        require: 'cant be empty'
    },
    date_of_donating: {
        type: String,
        require: 'cant be empty'
    }


});

AchieveSchema.pre('save', function(next) {
    next();
});

mongoose.model('Achievements', AchieveSchema);