const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');

var Rep = mongoose.model('Rep');
const Student = mongoose.model('Student');
const Others = mongoose.model('Others');

passport.use(
    new localStrategy({ usernameField: 'email' },
        (username, password, done) => {
            Rep.findOne({ email: username },
                (err, user) => {
                    if (err)
                        return done(err);
                    // unknown user
                    else if (!user) {
                        // return done(null, false, { message: 'Email is not registered' });
                        Student.findOne({ email: username },
                            (err, user) => {
                                if (err)
                                    return done(err);
                                // unknown user
                                else if (!user) {
                                    // return done(null, false, { message: 'Email is not registered' });
                                    Others.findOne({ email: username },
                                        (err, user) => {
                                            if (err)
                                                return done(err);
                                            // unknown user
                                            else if (!user){
                                                return done(null, false, { message: 'Email is not registered' });
                                            }
                                               
                                            // wrong password
                                            else if (!user.verifyPassword(password))
                                                return done(null, false, { message: 'Wrong password.' });
                                            // authentication succeeded
                                            else    
                                                return done(null, user);
                                        });
                                }

                                // wrong password
                                else if (!user.verifyPassword(password))
                                    return done(null, false, { message: 'Wrong password.' });
                                // authentication succeeded
                                else
                                    return done(null, user);
                            });
                    }

                    // wrong password
                    else if (!user.verifyPassword(password))
                        return done(null, false, { message: 'Wrong password.' });
                    // authentication succeeded
                    else
                        return done(null, user);
                });

        })
);  