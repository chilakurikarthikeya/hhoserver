(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/aboutus/aboutus.component.css":
/*!***********************************************!*\
  !*** ./src/app/aboutus/aboutus.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-card {\n    max-width: 400px;\n}\n\n.example-header-image {\n    background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\n    background-size: cover;\n}"

/***/ }),

/***/ "./src/app/aboutus/aboutus.component.html":
/*!************************************************!*\
  !*** ./src/app/aboutus/aboutus.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br>\n<div class=\"text-center\">\n    <img src=\"../../app/../assets/hho_logo.jpeg\" class=\"rounded mx-auto d-block hoverable\" alt=\"hho_logo\" height=\"128px\" width=\"128px\">\n</div>\n<br>\n<p class=\"text-center font-weight-bold\">About HHO</p>\n\n\n\n\n\n\n\n\n<mat-accordion>\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p style=\"text-align:left\"><strong>Vision</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n\n        <p style=\"color:#f60206;font-size:18px;text-align:center\"> <br/> <strong>\"Someone cannot Help Everyone,</strong><br/> <strong>But everyone can Help Someone.\"</strong> </p>\n        <p style=\"text-align:center\">We the HHO strive to bring unity within the students of RKV and try to overcome the problems faced. We mainly concentrate with the problems of physically disabled, orphans, aged ones and students in emergencies. <br/></p>\n\n    </mat-expansion-panel>\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p style=\"text-align:left\"><strong>ADVISORY PANEL</strong></p>\n\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <p style=\"text-align:center\">It is totally organized by students of RKV. The role of chairperson acts by the Director in-charge of our university.</p>\n        <p style=\"text-align:center\">It is totally organized by students of RKV. The role of chairperson acts by the Director in-charge of our university.</p>\n\n        <!-- /wp:paragraph -->\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph {\"align\":\"center\"} -->\n        <!-- wp:paragraph {\"align\":\"center\"} -->\n\n        <p style=\"text-align:center\">A pair of students come up from each class as representatives. They bring out the problems faced by the students to our knowledge. They collect the money and different funds in time. All the representatives come to gather with a discussion and\n            reasonably help them with the treasured funds.</p>\n    </mat-expansion-panel>\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p style=\"text-align:left\"><strong>RULES AND REGULATIONS</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <p>1. The victim should belong to RK Valley.</p>\n\n\n        <p>2. Any problem raised is manually verified, either personally or with any documentation.</p>\n\n\n        <p>3. Observing the victims financial status and comparing our financial status, an appropriate amount is donated.</p>\n\n\n        <p>4. When we face multiple problems, based on the time and severity of problem, the amount to be donated is priored.</p>\n\n\n        <p>5.Amount will be given to the needy victim based on the majority HR's opinion.<br/></p>\n\n\n        <p>6. Every expenditure is documented</p>\n\n\n        <p>For exit case of any solved health issue, we preserve all the paid bills and Exchequer details spent.</p>\n\n\n        <p>7. Any transaction from organization to the victim is done in the form of Exchequer or transferred to their account.</p>\n\n\n        <p>8. The amount preserved is only used for organization purposes. No personal expenditures are allowed.</p>\n\n\n        <p>9. A regular meeting is to be held on the every second Sunday of a month. In any emergencies, a temporary meeting can be called.</p>\n\n        <p>10. Any spontaneous decisions can be taken with the severity in the health issue with every HR'S opinion in their discussion.</p>\n\n\n        <p>11. Any modifications done within the organization it should be done in presence of every HR.</p>\n\n\n        <p>In case of any absence of HRs, it is modified under the supervision of available HHO members.</p>\n\n\n        <p>12. Individual groups within students is not entertained. Any issue raised, it should be brought into the notice of HRS of the particular class.</p>\n\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>INITIATIVE DOCUMENTATION</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <p style=\"text-align:center\">Before going onto a discussion of any arose problem, we go through verification of documents like</p>\n        <p>->If any health issue, the medical reports from a referred doctor are to be submitted.</p>\n        <p>->If the victim is of any natural calamity, the basic and unique identities like Aadhar and ration card are to be submitted.</p>\n        <p>->For any problem, all the income sources are verified keenly.</p>\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>WAY OF SOLVING ANY PROBLEM</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <p style=\"text-align: center;\">If any problems arises, it is intimated to the class HR by the student. The problem is taken to the executive and core representatives of HHO. The initiative documentation is verified among them, and finally a discussion is called.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph {\"align\":\"left\"} -->\n        <p style=\"text-align: left;\"><strong>In the discussion....</strong></p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph {\"align\":\"center\"} -->\n        <p style=\"text-align: center;\">The financial status of the victim is observed and we compare our financial status in HHO account. Having a discussion within the HHO members, we come up with an appropriate help to the victim.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph {\"align\":\"center\"} -->\n        <p style=\"text-align: center;\">We personally meet the victims and their family members to have a detail structure of their living. We in personal give away the money in form of cheque to the victim. No just the monitory help but we whole unitedly withstand and support in every\n            aspect to come out of the problem by providing many other solutions too.</p>\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>FUNDS RAISED</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <!-- wp:paragraph {\"align\":\"center\"} -->\n        <p style=\"text-align: center;\">To overcome any problem, our organization get a full-fledged support from the students, the lecturers, mentors and other faculty members. We are also supported by the security groups and many other non-teaching staffs.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>*Competitions like essay writing, quizzes etc., fun games are organized in the events held out university as an income source.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>*Funds from Alumnus.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>*Each and every student contribute maximum ₹10/- per month.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>* From donation boxes.</p>\n        <!-- /wp:paragraph -->\n\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>PROMOTING</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <!-- wp:paragraph -->\n        <p>-&gt;An HRS promote about the organization in their respective classes.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>-&gt; Events are organized.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>→ every update is displayed in the form notices.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>→ A website is launched “https//:hhorkv.in” with different services.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>→ The social media FB, Twitter and whatsoever used to promote share our achievements, and to pass the information within the group.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>→ Quotations, Drawings, Short film and any other art works are encouraged to promote HHO.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>→ Articles are also published in regional newspapers.</p>\n        <!-- /wp:paragraph -->\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>INCITERS</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n        <p style=\"text-align: center;\">We are mainly encouraged by the students, the administration dept. and both teaching and non-teaching staffs in RKV.<br />We are very thankful for the above mentioned for always being the kith and kin in every aspect. We are grateful for all the\n            encouragement and support.</p>\n    </mat-expansion-panel>\n\n\n    <mat-expansion-panel>\n        <mat-expansion-panel-header>\n            <mat-panel-title>\n                <p><strong>SPECIAL CASES</strong></p>\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n\n        <!-- wp:paragraph -->\n        <p>1. When we have sufficient amount, we are enthusiastic to donate to external issues.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>2. Provide basic necessities for the orphans.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>3. In case of any natural calamity, we act as a medium to them with money or any other reasonable commodities.</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>&nbsp;</p>\n        <!-- /wp:paragraph -->\n\n        <!-- wp:paragraph -->\n        <p>&nbsp;</p>\n        <!-- /wp:paragraph -->\n    </mat-expansion-panel>\n</mat-accordion>"

/***/ }),

/***/ "./src/app/aboutus/aboutus.component.ts":
/*!**********************************************!*\
  !*** ./src/app/aboutus/aboutus.component.ts ***!
  \**********************************************/
/*! exports provided: AboutusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusComponent", function() { return AboutusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutusComponent = /** @class */ (function () {
    function AboutusComponent() {
        this.panelOpenState = false;
    }
    AboutusComponent.prototype.ngOnInit = function () {
    };
    AboutusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-aboutus',
            template: __webpack_require__(/*! ./aboutus.component.html */ "./src/app/aboutus/aboutus.component.html"),
            styles: [__webpack_require__(/*! ./aboutus.component.css */ "./src/app/aboutus/aboutus.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutusComponent);
    return AboutusComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<!-- <app-main-nav> -->\n<router-outlet></router-outlet>\n<!-- </app-main-nav> -->\n\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'hho';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");
/* harmony import */ var _auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth/auth.interceptor */ "./src/app/auth/auth.interceptor.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./material */ "./src/app/material.ts");
/* harmony import */ var _componentname_componentname_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./componentname/componentname.component */ "./src/app/componentname/componentname.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./aboutus/aboutus.component */ "./src/app/aboutus/aboutus.component.ts");
/* harmony import */ var _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./contactus/contactus.component */ "./src/app/contactus/contactus.component.ts");
/* harmony import */ var _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./my-nav/my-nav.component */ "./src/app/my-nav/my-nav.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./main-nav/main-nav.component */ "./src/app/main-nav/main-nav.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_problem_posting_problem_posting_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/problem-posting/problem-posting.component */ "./src/app/components/problem-posting/problem-posting.component.ts");
/* harmony import */ var _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/user-profile/user-profile.component */ "./src/app/components/user-profile/user-profile.component.ts");
/* harmony import */ var _team_team_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./team/team.component */ "./src/app/team/team.component.ts");
/* harmony import */ var ngx_loading__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-loading */ "./node_modules/ngx-loading/fesm5/ngx-loading.js");
/* harmony import */ var angular_material_fileupload__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! angular-material-fileupload */ "./node_modules/angular-material-fileupload/matFileUpload.esm.js");
/* harmony import */ var _components_achievements_achievements_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/achievements/achievements.component */ "./src/app/components/achievements/achievements.component.ts");
/* harmony import */ var ngx_flip__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ngx-flip */ "./node_modules/ngx-flip/fesm5/ngx-flip.js");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(angularfire2__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(angularfire2_storage__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var _components_achievement_post_achievement_post_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/achievement-post/achievement-post.component */ "./src/app/components/achievement-post/achievement-post.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// materil design




















// loaing

// file upload


// flipping card

// firebase



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _componentname_componentname_component__WEBPACK_IMPORTED_MODULE_8__["ComponentnameComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_10__["FooterComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_12__["AboutusComponent"],
                _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_13__["ContactusComponent"],
                _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_14__["MyNavComponent"],
                _main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_17__["MainNavComponent"],
                _components_register_register_component__WEBPACK_IMPORTED_MODULE_18__["RegisterComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_19__["LoginComponent"],
                _components_problem_posting_problem_posting_component__WEBPACK_IMPORTED_MODULE_23__["ProblemPostingComponent"],
                _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_24__["UserProfileComponent"],
                _team_team_component__WEBPACK_IMPORTED_MODULE_25__["TeamComponent"],
                _components_achievements_achievements_component__WEBPACK_IMPORTED_MODULE_28__["AchievementsComponent"],
                _components_achievement_post_achievement_post_component__WEBPACK_IMPORTED_MODULE_32__["AchievementPostComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _material__WEBPACK_IMPORTED_MODULE_7__["MaterialModule"],
                // fire base
                angularfire2__WEBPACK_IMPORTED_MODULE_30__["AngularFireModule"].initializeApp({
                    apiKey: 'AIzaSyBmnCO9E7Jt_eecG8toMbvfD4g4VJ5Kmt8',
                    authDomain: 'hho-rkv.firebaseapp.com',
                    databaseURL: 'https://hho-rkv.firebaseio.com',
                    projectId: 'hho-rkv',
                    storageBucket: 'hho-rkv.appspot.com',
                    messagingSenderId: '355204344788'
                }),
                angularfire2_storage__WEBPACK_IMPORTED_MODULE_31__["AngularFireStorageModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot([
                    { path: '', redirectTo: '/home', pathMatch: 'full' },
                    { path: 'aboutus', component: _aboutus_aboutus_component__WEBPACK_IMPORTED_MODULE_12__["AboutusComponent"] },
                    { path: 'contactus', component: _contactus_contactus_component__WEBPACK_IMPORTED_MODULE_13__["ContactusComponent"] },
                    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"] },
                    { path: 'my-nav', component: _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_14__["MyNavComponent"] },
                    { path: 'main- nav', component: _main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_17__["MainNavComponent"] },
                    { path: 'register', component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_18__["RegisterComponent"] },
                    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_19__["LoginComponent"] },
                    { path: 'post', component: _components_problem_posting_problem_posting_component__WEBPACK_IMPORTED_MODULE_23__["ProblemPostingComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
                    { path: 'user_profile', component: _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_24__["UserProfileComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]] },
                    { path: 'team', component: _team_team_component__WEBPACK_IMPORTED_MODULE_25__["TeamComponent"] },
                    { path: 'achievements', component: _components_achievements_achievements_component__WEBPACK_IMPORTED_MODULE_28__["AchievementsComponent"] },
                    { path: 'achieve_post', component: _components_achievement_post_achievement_post_component__WEBPACK_IMPORTED_MODULE_32__["AchievementPostComponent"] }
                ]),
                ngx_loading__WEBPACK_IMPORTED_MODULE_26__["NgxLoadingModule"].forRoot({}),
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_15__["LayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_16__["MatListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_20__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_20__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HttpClientModule"],
                angular_material_fileupload__WEBPACK_IMPORTED_MODULE_27__["MatFileUploadModule"],
                ngx_flip__WEBPACK_IMPORTED_MODULE_29__["FlipModule"],
            ],
            providers: [{
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_22__["HTTP_INTERCEPTORS"],
                    useClass: _auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_5__["AuthInterceptor"],
                    multi: true
                }, _auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"], _users_service__WEBPACK_IMPORTED_MODULE_21__["UsersService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, usersService) {
        this.router = router;
        this.usersService = usersService;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (!this.usersService.isLoggedIn()) {
            this.router.navigateByUrl('/login');
            this.usersService.deleteToken();
        }
        return true;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/auth/auth.interceptor.ts":
/*!******************************************!*\
  !*** ./src/app/auth/auth.interceptor.ts ***!
  \******************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(router, usersService) {
        this.router = router;
        this.usersService = usersService;
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        if (req.headers.get('noauth'))
            return next.handle(req.clone());
        else {
            var clonedreq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + this.usersService.getToken())
            });
            return next.handle(clonedreq).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(function (event) { }, function (err) {
                if (err.error.auth == false) {
                    _this.router.navigateByUrl('/login');
                }
            }));
        }
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/componentname/componentname.component.css":
/*!***********************************************************!*\
  !*** ./src/app/componentname/componentname.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/componentname/componentname.component.html":
/*!************************************************************!*\
  !*** ./src/app/componentname/componentname.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  componentname works!\n</p>\n"

/***/ }),

/***/ "./src/app/componentname/componentname.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/componentname/componentname.component.ts ***!
  \**********************************************************/
/*! exports provided: ComponentnameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentnameComponent", function() { return ComponentnameComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ComponentnameComponent = /** @class */ (function () {
    function ComponentnameComponent() {
    }
    ComponentnameComponent.prototype.ngOnInit = function () {
    };
    ComponentnameComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-componentname',
            template: __webpack_require__(/*! ./componentname.component.html */ "./src/app/componentname/componentname.component.html"),
            styles: [__webpack_require__(/*! ./componentname.component.css */ "./src/app/componentname/componentname.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ComponentnameComponent);
    return ComponentnameComponent;
}());



/***/ }),

/***/ "./src/app/components/achievement-post/achievement-post.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/achievement-post/achievement-post.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/achievement-post/achievement-post.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/achievement-post/achievement-post.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"card\">\n        <div class=\"card-header\">\n            Firebase Cloud Storage & Angular 5\n        </div>\n        <div class=\"card-body\">\n            <form [formGroup]=\"achieveForm\" (ngSubmit)=\"onpost()\">\n                <img [src]=\"url\" height=\"200\"> <br/>\n                <h5 class=\"card-title\">Select a file for upload:</h5>\n                <input type=\"file\" (change)=\"upload($event)\" accept=\".png,.jpg\" required/>\n\n                <div class=\"progress\">\n                    <div class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadProgress | async) + '%'\" [attr.aria-valuenow]=\"(uploadProgress | async)\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n                url:<input type=\"text\" value={{imageurl}} [(ngModel)]=\"imageurl\" formControlName=\"imageUrl\" [ngClass]=\"{'is-invalid': submitted && f.imageUrl.errors}\" required>\n                <br>\n                <mat-form-field>\n                    <input matInput placeholder=\"Name\" formControlName=\"name\" [ngClass]=\"{'is-invalid': submitted && f.name.errors}\" required>\n                </mat-form-field>\n                <br>\n                <mat-form-field>\n                    <input matInput placeholder=\"Problem Name\" formControlName=\"problem\" [ngClass]=\"{'is-invalid': submitted && f.problem.errors}\" required>\n                </mat-form-field>\n                <br>\n                <mat-form-field>\n                    <input matInput placeholder=\"Amount Donated\" formControlName=\"amount_donated\" [ngClass]=\"{'is-invalid': submitted && f.amount_donated.errors}\" required>\n                </mat-form-field>\n                <br>\n                <mat-form-field>\n                    <input matInput [matDatepicker]=\"picker\" placeholder=\"Date Of donating\" formControlName=\"date_of_donating\" [ngClass]=\"{'is-invalid': submitted && f.date_of_donating.errors}\" required>\n                    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n                    <mat-datepicker #picker></mat-datepicker>\n                </mat-form-field>\n                <p class=\"\">\n                    <button mat-flat-button color=\"primary\">Post</button>\n                </p>\n\n            </form>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/achievement-post/achievement-post.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/achievement-post/achievement-post.component.ts ***!
  \***************************************************************************/
/*! exports provided: AchievementPostComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AchievementPostComponent", function() { return AchievementPostComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../users.service */ "./src/app/users.service.ts");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularfire2/storage */ "./node_modules/angularfire2/storage/index.js");
/* harmony import */ var angularfire2_storage__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// firebase


// import { url } from 'inspector';
// import { FormBuilder, Validators, FormGroup } from '@angular/forms';
var AchievementPostComponent = /** @class */ (function () {
    function AchievementPostComponent(afStorage, usersService, router, formBuilder) {
        this.afStorage = afStorage;
        this.usersService = usersService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.url = '';
        this.submitted = false;
    }
    AchievementPostComponent.prototype.upload = function (event) {
        var _this = this;
        this.url = event.target.files[0].name;
        var id = this.url;
        this.ref = this.afStorage.ref(id);
        this.task = this.ref.put(event.target.files[0]);
        this.uploadProgress = this.task.percentageChanges();
        // this.downloadURL = this.task.downloadURL();
        this.downloadURL = this.ref.getDownloadURL();
        console.log(this.downloadURL);
        this.downloadURL.subscribe(function (res) {
            return console.log(_this.imageurl = res);
        });
        // console.log('comp');
        // console.log(this.imageurl);
        // this.url = this.imageurl;
        // for image preview
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = function (event) {
                _this.url = event.target.result;
            };
        }
    };
    AchievementPostComponent.prototype.ngOnInit = function () {
        this.achieveForm = this.formBuilder.group({
            imageUrl: [this.imageurl, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            problem: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            amount_donated: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            date_of_donating: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    Object.defineProperty(AchievementPostComponent.prototype, "f", {
        get: function () { return this.achieveForm.controls; },
        enumerable: true,
        configurable: true
    });
    AchievementPostComponent.prototype.onpost = function () {
        this.submitted = true;
        if (this.achieveForm.invalid) {
            console.log(this.achieveForm.value);
            // alert('gusss...');
            return;
        }
        this.usersService.achievepost(this.achieveForm.value).subscribe(function (res) {
            alert('successfully posted');
        }, function (err) {
            alert('error while posting');
        });
    };
    AchievementPostComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-achievement-post',
            template: __webpack_require__(/*! ./achievement-post.component.html */ "./src/app/components/achievement-post/achievement-post.component.html"),
            styles: [__webpack_require__(/*! ./achievement-post.component.css */ "./src/app/components/achievement-post/achievement-post.component.css")]
        }),
        __metadata("design:paramtypes", [angularfire2_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"], _users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], AchievementPostComponent);
    return AchievementPostComponent;
}());



/***/ }),

/***/ "./src/app/components/achievements/achievements.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/achievements/achievements.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-card {\n    /* max-width: 400px; */\n    max-width: 362px;\n    max-height: 363px;\n}\n\n.example-header-image {\n    background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\n    background-size: cover;\n}"

/***/ }),

/***/ "./src/app/components/achievements/achievements.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/achievements/achievements.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p *ngFor=\"let a of achieve\">\n    <img [src]=\"{{achieve[1].imageUrl}}\" height=\"100px\" width=\"100px\">\n</p> -->\n\n<!-- <ul>\n    <li *ngFor=\"let element of achieve\">\n\n        <mat-card class=\"example-card\">\n            <img mat-card-image src=\"{{element.imageUrl}}\" alt=\"loading\" width=\"300px\" height=\"300px\">\n        </mat-card> -->\n<!-- <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\">\n        <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\">\n        <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\">\n        <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\">\n        <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\">\n        <img src=\"{{element.imageUrl}}\" height=\"100px\" width=\"100px\"> -->\n<!-- </li>\n</ul> -->\n<div class=\"container\" style=\"margin-left: 0px;\">\n    <div class=\"row\">\n        <div class=\"col-md-3\" *ngFor=\"let element of achieve.slice().reverse()\">\n            <mat-card class=\"example-card\">\n                <img mat-card-image src=\"{{element.imageUrl}}\" alt=\"Photo of a Shiba Inu\" width=\"362px\" height=\"262px\">\n                <mat-card-content>\n                    Name:{{element.name}} <br> problem:{{element.problem}} <br>\n                </mat-card-content>\n            </mat-card>\n            <br>\n\n        </div>\n    </div>\n</div>\n\n\n\n<!-- <mat-card class=\"example-card\">\n    <mat-card-header>                                                                                                                                              \n        <div mat-card-avatar class=\"example-header-image\"></div>\n    </mat-card-header>\n    <img mat-card-image src=\"{{element.imageUrl}}\" alt=\"Photo of a Shiba Inu\" width=\"300px\" height=\"300px\">\n    <mat-card-content>\n        <p>\n            The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting.\n        </p>\n    </mat-card-content>\n</mat-card> -->"

/***/ }),

/***/ "./src/app/components/achievements/achievements.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/achievements/achievements.component.ts ***!
  \*******************************************************************/
/*! exports provided: AchievementsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AchievementsComponent", function() { return AchievementsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AchievementsComponent = /** @class */ (function () {
    function AchievementsComponent(usersService, router, formBuilder) {
        this.usersService = usersService;
        this.router = router;
        this.formBuilder = formBuilder;
    }
    AchievementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.achievements().subscribe(function (res) {
            // console.log(res['achieve']);
            // console.log('success');
            _this.achieve = res['achieve'];
            // console.log(this.achieve[1].name);
            // console.log(this.achieve.length);
        }, function (err) {
            console.log('error  ');
        });
    };
    AchievementsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-achievements',
            template: __webpack_require__(/*! ./achievements.component.html */ "./src/app/components/achievements/achievements.component.html"),
            styles: [__webpack_require__(/*! ./achievements.component.css */ "./src/app/components/achievements/achievements.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], AchievementsComponent);
    return AchievementsComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center_div {\n    margin: 0 auto;\n    width: 50%/* value of your choice which suits your alignment */\n}\nbody {\n    margin: 40px;\n  }\n.tot\n{\n    padding-left: 30%;\n    /* float: center; */\n}\nh2\n{\n    text-align: center;\n}\n.example-large-box {\n    height: 400px;\n    width: 54%;\n  }\n.short\n  {\n      text-align: center;\n  }\n.wrapper {\n      display: -ms-grid;\n      display: grid;\n      grid-gap: 10px;\n          -ms-grid-columns: 20% 40% 40%;\n              grid-template-columns: 20% 40% 40%;\n          -ms-grid-rows: 200px 600%;\n              grid-template-rows: 200px 600%;\n          background-color: rgb(194, 189, 189);\n          color: #444;\n      }\n/* .box {\n          background-color: #444;\n          color: #fff;\n          border-radius: 5px;\n          padding: 20px;\n          font-size: 150%;\n  \n      }\n  */\n.a {\n          -ms-grid-column: 1;\n          -ms-grid-column-span: 2;\n          grid-column: 1 / 3;\n          -ms-grid-row: 1;\n          grid-row: 1;\n      }\n.b {\n          -ms-grid-column: 3 ;\n          grid-column: 3 ;\n          -ms-grid-row: 1;\n          -ms-grid-row-span: 2;\n          grid-row: 1 / 3;\n          text-align: center;\n      }\n.c {\n          -ms-grid-column: 1 ;\n          grid-column: 1 ;\n          -ms-grid-row: 2 ;\n          grid-row: 2 ;\n      }\n.d {\n          -ms-grid-column: 2;\n          grid-column: 2;\n          -ms-grid-row: 2;\n          grid-row: 2;\n      }"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3 class=\"text-center\">Login Form</h3>\n\n<div class=\"container\">\n\n    <div class=\"row align-items-center\">\n        <div class=\"col\">\n\n        </div>\n        <div class=\"col\">\n            <mat-card>\n                <form [formGroup]=\"loginForm\" (ngSubmit)=\"onlogin()\">\n                    <mat-form-field>\n                        <input matInput placeholder=\"Enter your email\" formControlName=\"email\" [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\" required>\n                        <!-- <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error> -->\n                    </mat-form-field>\n                    <br>\n                    <mat-form-field>\n                        <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" required>\n                        <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        <!-- <mat-error *ngIf=\"password.invalid\">{{getErrorMessage1()}}</mat-error> -->\n                    </mat-form-field>\n                    <p class=\"text-center\">\n                        <button mat-flat-button color=\"primary\">Login</button>\n                    </p>\n                </form>\n            </mat-card>\n\n            <!-- <div class=\"my-container\"> -->\n            <!-- <ng-template #customLoadingTemplate>\n                    <div class=\"custom-class\">\n                        <h3>\n                            Loading...\n                        </h3>\n                        <button (click)=\"showAlert()\">\n                            Click me!\n                        </button>\n                    </div>\n                </ng-template> -->\n\n            <ngx-loading [show]=\"loading\" [config]=\"{ backdropBorderRadius: '10px' }\" [template]=\"customLoadingTemplate\"></ngx-loading>\n\n            <!-- </div> -->\n        </div>\n        <div class=\"col\">\n\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../users.service */ "./src/app/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    // getErrorMessage() {
    //   return this.email.hasError('required') ? 'You must enter a value' :
    //       this.email.hasError('email') ? 'Not a valid email' :
    //           '';
    // }
    // getErrorMessage1() {
    //   return this.password.hasError('required') ? 'You must enter a value' :
    //       // this.email.hasError('email') ? 'Not a valid email' :
    //           '';
    // }
    function LoginComponent(router, usersService, formBuilder) {
        this.router = router;
        this.usersService = usersService;
        this.formBuilder = formBuilder;
        this.submitted = false;
        this.hide = true;
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.usersService.isLoggedIn()) {
            this.router.navigateByUrl('/user_profile');
        }
        this.loginForm = this.formBuilder.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(6)]]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.onlogin = function () {
        var _this = this;
        this.submitted = true;
        this.loading = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            this.loading = false;
            return;
        }
        this.usersService.login(this.loginForm.value).subscribe(function (res) {
            _this.loading = false;
            _this.usersService.setToken(res['token']);
            _this.router.navigateByUrl('/user_profile');
            // console.log(form.value);
        }, function (err) {
            // this.serverErrorMessages = err.console.error.message;
            alert('error while loging in');
            _this.loading = false;
            // console.log(form.value);
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/problem-posting/problem-posting.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/problem-posting/problem-posting.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".centerr {\n    margin: auto;\n    width: 50%;\n    align-content: center;\n    /* value of your choice which suits your alignment */\n}\n\n.example-small-box,\n.example-large-box {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    margin: 16px;\n    padding: 16px;\n    border-radius: 8px;\n}\n\n.example-full-width {\n    width: 80%;\n}\n\n.example-ful-width {\n    width: 40%;\n}\n\n.example-large-box {\n    height: 600px;\n    width: 700px;\n}\n\nh3 {\n    margin: auto;\n    padding-left: 200px;\n}\n\n.example-card {\n    max-width: 400px;\n}\n\n.example-header-image {\n    background-image: url('https://material.angular.io/assets/img/examples/shiba1.jpg');\n    background-size: cover;\n}"

/***/ }),

/***/ "./src/app/components/problem-posting/problem-posting.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/problem-posting/problem-posting.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Purple Header -->\n<div class=\"edge-header deep-purple\"></div>\n\n<!-- Main Container -->\n<div class=\"container free-bird\">\n    <div class=\"row\">\n        <div class=\"col-md-8 col-lg-7 mx-auto float-none white z-depth-1 py-2 px-2\">\n\n            <!--Naked Form-->\n            <div class=\"card-body\">\n                <h2 class=\"h2-responsive\"><strong>Problem posting</strong></h2>\n                <p class=\"pb-4\">Problem posting</p>\n\n                <!--Body-->\n                <form action=\"#\">\n\n\n\n                    <h5 class=\"h5-responsive\">Victim Name</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-user prefix\"></i>\n                        <input type=\"text\" id=\"form2\" class=\"form-control\">\n                        <label for=\"form2\"></label>\n                    </div>\n\n                    <h5 class=\"h5-responsive\">University ID No</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-id-badge prefix\"></i>\n                        <input type=\"text\" id=\"form2\" class=\"form-control\">\n                        <label for=\"form2\"></label>\n                    </div>\n\n                    <h5 class=\"h5-responsive\">Email ID</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-envelope prefix\"></i>\n                        <input type=\"text\" id=\"form2\" class=\"form-control\">\n                        <label for=\"form2\"></label>\n                    </div>\n\n                    <h5 class=\"h5-responsive\">Phone No</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-phone prefix\"></i>\n                        <input type=\"text\" id=\"form2\" class=\"form-control\">\n                        <label for=\"form2\"></label>\n                    </div>\n\n                    <h5 class=\"h5-responsive\">Branch & classs</h5>\n                    <div class=\"md-form\">\n                        <mat-form-field>\n                            <mat-select placeholder=\"Select Branch\" required>\n                                <mat-option *ngFor=\"let branch of branchs\" [value]=\"branch.value\">\n                                    {{branch.viewValue}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- <h5 class=\"h5-responsive\">Branch</h5> -->\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"8\" placeholder=\"Class(eg.AB1G001)\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/8</mat-hint>\n                        </mat-form-field>\n\n                    </div>\n\n                    <h5 class=\"h5-responsive\">Problem</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-plus-square prefix\"></i>\n                        <input type=\"text\" id=\"form2\" class=\"form-control\">\n                        <label for=\"form2\"></label>\n                    </div>\n                    <!--Textarea with icon-->\n                    <h5 class=\"h5-responsive\">Problem Description</h5>\n                    <div class=\"md-form\">\n                        <i class=\"fa fa-pencil prefix\"></i>\n                        <textarea type=\"text\" id=\"form7\" class=\"md-textarea form-control\" rows=\"2\"></textarea>\n                        <label for=\"form7\"></label>\n                    </div>\n\n                    <h5 class=\"h5-responsive\">Upload Supported Documents</h5>\n                    <p>(if more than one file compress file to any forment then upload )</p>\n                    <div class=\"md-form\">\n                        <label for=\"singleFile\"></label>\n                        <input id=\"singleFile\" type=\"file\" />\n                    </div>\n\n\n                    <div class=\"text-xs-left\">\n                        <button class=\"btn btn-primary\">Submit</button>\n                    </div>\n                </form>\n\n\n\n            </div>\n            <!--Naked Form-->\n\n        </div>\n    </div>\n</div>\n<!-- /.Main Container -->"

/***/ }),

/***/ "./src/app/components/problem-posting/problem-posting.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/problem-posting/problem-posting.component.ts ***!
  \*************************************************************************/
/*! exports provided: ProblemPostingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProblemPostingComponent", function() { return ProblemPostingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProblemPostingComponent = /** @class */ (function () {
    function ProblemPostingComponent() {
        // Branch
        this.branchs = [
            { value: 'CSE', viewValue: 'CSE' },
            { value: 'ECE', viewValue: 'ECE' },
            { value: 'CIVIL', viewValue: 'CIVIL' },
            { value: 'MECH', viewValue: 'MECH' },
            { value: 'MME', viewValue: 'MME' },
            { value: 'CHEM', viewValue: 'CHEM' }
        ];
    }
    ProblemPostingComponent.prototype.ngOnInit = function () {
    };
    ProblemPostingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-problem-posting',
            template: __webpack_require__(/*! ./problem-posting.component.html */ "./src/app/components/problem-posting/problem-posting.component.html"),
            styles: [__webpack_require__(/*! ./problem-posting.component.css */ "./src/app/components/problem-posting/problem-posting.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProblemPostingComponent);
    return ProblemPostingComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/register/register.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example {\n    width: 55%;\n}\n\n.example>* {\n    width: 100%;\n}\n\n.example-container {\n    display: flex;\n    flex-direction: column;\n}\n\n.example-container>* {\n    width: 100%;\n}"

/***/ }),

/***/ "./src/app/components/register/register.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/register/register.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col\">\n            1 of 2\n        </div>\n        <div class=\"col\">\n            2 of 2\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col\">\n            1 of 3\n        </div>\n        <div class=\"col\">\n            2 of 3\n        </div>\n        <div class=\"col\">\n            <h3 class=\"text-center\"> REGISTRATION </h3>\n            <mat-tab-group>\n                <mat-tab label=\"Representative\">\n\n\n                    <form [formGroup]=\"HRForm\" (ngSubmit)=\"onHrSubmit()\">\n\n                        <mat-form-field appearance=\"legacy\" class=\"example\">\n                            <mat-label>Full Name</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"fullname\" [ngClass]=\"{'is-invalid':hrSubmitted && f.fullname.errors}\" required>\n                            <mat-icon matSuffix>account_circle</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"7\" placeholder=\"ID No\" formControlName=\"id\" [ngClass]=\"{'is-invalid':hrSubmitted && f.id.errors}\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/7</mat-hint>\n                        </mat-form-field>\n                        <!-- email -->\n                        <mat-form-field class=\"example\">\n                            <input matInput placeholder=\"Email ID\" formControlName=\"email\" [ngClass]=\"{'is-invalid':hrSubmitted && f.email.errors}\" required>\n                            <mat-icon matSuffix>email</mat-icon>\n                            <!-- <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error> -->\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- phone num -->\n                        <mat-form-field appearance=\"legacy\">\n                            <mat-label>Phone_no</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"ph_no\" [ngClass]=\"{'is-invalid':hrSubmitted && f.ph_no.errors}\" required>\n                            <mat-icon matSuffix>phone</mat-icon>\n                        </mat-form-field>\n                        <!-- class -->\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"8\" placeholder=\"Class\" formControlName=\"class\" [ngClass]=\"{'is-invalid':hrSubmitted && f.class.errors}\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/8</mat-hint>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- branch -->\n                        <mat-form-field>\n                            <mat-select placeholder=\"Select Branch\" formControlName=\"branch\" [ngClass]=\"{'is-invalid':hrSubmitted && f.branch.errors}\" required>\n                                <mat-option *ngFor=\"let branch of branchs\" [value]=\"branch.value\">\n                                    {{branch.viewValue}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n\n                        &nbsp;\n                        <!-- designation -->\n                        <mat-form-field>\n                            <mat-select placeholder=\"Designation\" formControlName=\"designation\" [ngClass]=\"{'is-invalid':hrSubmitted && f.designation.errors}\" required>\n                                <mat-option *ngFor=\"let designation of desig\" [value]=\"designation.value\">\n                                    {{designation.viewValue}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- date of joing -->\n                        <mat-form-field>\n                            <input matInput [matDatepicker]=\"picker\" placeholder=\"Date Of Joining\" formControlName=\"date_of_joining\" [ngClass]=\"{'is-invalid':hrSubmitted && f.date_of_joining.errors}\" required>\n                            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n                            <mat-datepicker #picker></mat-datepicker>\n                        </mat-form-field>\n                        <!-- moto of joinging -->\n                        <mat-form-field class=\"example-container\">\n                            <textarea matInput placeholder=\"Moto of joinging\" formControlName=\"moto_of_joining\" [ngClass]=\"{'is-invalid':hrSubmitted && f.moto_of_joining.errors}\" required></textarea>\n                        </mat-form-field>\n                        <!-- pswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\" password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\" [ngClass]=\"{'is-invalid':hrSubmitted && f.password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- cpswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\"conform password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"c_password\" [ngClass]=\"{'is-invalid':hrSubmitted && f.c_password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n\n                        <p class=\"text-center\">\n                            <button mat-flat-button color=\"primary\">Register</button>\n                        </p>\n\n                    </form>\n\n\n\n\n\n\n\n\n                </mat-tab>\n\n                <mat-tab label=\"Student \">\n\n                    <form [formGroup]=\"StuForm\" (ngSubmit)=\"onStuSubmit()\">\n\n                        <mat-form-field appearance=\"legacy\" class=\"example\">\n                            <mat-label>Full Name</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"fullname\" [ngClass]=\"{'is-invalid':StuSubmitted && f.fullname.errors}\" required>\n                            <mat-icon matSuffix>account_circle</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"7\" placeholder=\"ID No\" formControlName=\"id\" [ngClass]=\"{'is-invalid':StuSubmitted && f.id.errors}\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/7</mat-hint>\n                        </mat-form-field>\n                        <!-- email -->\n                        <mat-form-field class=\"example\">\n                            <input matInput placeholder=\"Email ID\" formControlName=\"email\" [ngClass]=\"{'is-invalid':StuSubmitted && f.email.errors}\" required>\n                            <mat-icon matSuffix>email</mat-icon>\n                            <!-- <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error> -->\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- phone num -->\n                        <mat-form-field appearance=\"legacy\">\n                            <mat-label>Phone_no</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"ph_no\" [ngClass]=\"{'is-invalid':StuSubmitted && f.ph_no.errors}\" required>\n                            <mat-icon matSuffix>phone</mat-icon>\n                        </mat-form-field>\n                        <!-- class -->\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"8\" placeholder=\"Class\" formControlName=\"class\" [ngClass]=\"{'is-invalid':StuSubmitted && f.class.errors}\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/8</mat-hint>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- branch -->\n                        <mat-form-field>\n                            <mat-select placeholder=\"Select Branch\" formControlName=\"branch\" [ngClass]=\"{'is-invalid':StuSubmitted && f.branch.errors}\" required>\n                                <mat-option *ngFor=\"let branch of branchs\" [value]=\"branch.value\">\n                                    {{branch.viewValue}}\n                                </mat-option>\n                            </mat-select>\n                        </mat-form-field>\n\n                        &nbsp;\n\n\n\n                        <!-- pswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\" password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\" [ngClass]=\"{'is-invalid':StuSubmitted && f.password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- cpswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\"conform password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"c_password\" [ngClass]=\"{'is-invalid':StuSubmitted && f.c_password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n\n                        <p class=\"text-center\">\n                            <button mat-flat-button color=\"primary\">Register</button>\n                        </p>\n\n                    </form>\n\n\n                </mat-tab>\n\n\n\n\n\n                <mat-tab label=\"Others\">\n\n\n                    <form [formGroup]=\"OtherForm\" (ngSubmit)=\"onOtherSubmit()\">\n\n                        <mat-form-field appearance=\"legacy\" class=\"example\">\n                            <mat-label>Full Name</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"fullname\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.fullname.errors}\" required>\n                            <mat-icon matSuffix>account_circle</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <mat-form-field>\n                            <input matInput #input maxlength=\"7\" placeholder=\"ID No\" formControlName=\"id\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.id.errors}\" required>\n                            <mat-hint align=\"end\">{{input.value?.length || 0}}/7</mat-hint>\n                        </mat-form-field>\n                        <!-- email -->\n                        <mat-form-field class=\"example\">\n                            <input matInput placeholder=\"Email ID\" formControlName=\"email\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.email.errors}\" required>\n                            <mat-icon matSuffix>email</mat-icon>\n                            <!-- <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error> -->\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- phone num -->\n                        <mat-form-field appearance=\"legacy\">\n                            <mat-label>Phone_no</mat-label>\n                            <input matInput placeholder=\"Placeholder\" formControlName=\"ph_no\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.ph_no.errors}\" required>\n                            <mat-icon matSuffix>phone</mat-icon>\n                        </mat-form-field>\n\n                        &nbsp;\n\n\n\n                        <!-- pswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\" password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n                        &nbsp;\n                        <!-- cpswd -->\n                        <mat-form-field>\n                            <input matInput placeholder=\"conform password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"c_password\" [ngClass]=\"{'is-invalid':OtherSubmitted && f.c_password.errors}\" required>\n                            <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\n                        </mat-form-field>\n\n                        <p class=\"text-center\">\n                            <button mat-flat-button color=\"primary\">Register</button>\n                        </p>\n\n                    </form>\n\n                </mat-tab>\n            </mat-tab-group>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/register/register.component.ts ***!
  \***********************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../users.service */ "./src/app/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterComponent = /** @class */ (function () {
    // mail validation
    // email = new FormControl('', [Validators.required, Validators.email]);
    // getErrorMessage() {
    //   return this.email.hasError('required') ? 'You must enter a value' :
    //       this.email.hasError('email') ? 'Not a valid email' :
    //           '';
    // }
    function RegisterComponent(usersService, router, formBuilder) {
        this.usersService = usersService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.hide = true;
        this.hrSubmitted = false;
        this.StuSubmitted = false;
        this.OtherSubmitted = false;
        // Branch
        this.branchs = [
            { value: 'CSE', viewValue: 'CSE' },
            { value: 'ECE', viewValue: 'ECE' },
            { value: 'CIVIL', viewValue: 'CIVIL' },
            { value: 'MECH', viewValue: 'MECH' },
            { value: 'MME', viewValue: 'MME' },
            { value: 'CHEM', viewValue: 'CHEM' }
        ];
        // Branch
        this.desig = [
            { value: 'Core', viewValue: 'Core' },
            { value: 'Accountant', viewValue: 'Accountant' },
            { value: 'Executive', viewValue: 'Executive' },
            { value: 'Representative', viewValue: 'Representative' }
        ];
    }
    RegisterComponent.prototype.ngOnInit = function () {
        if (this.usersService.isLoggedIn()) {
            this.router.navigateByUrl('/user_profile');
        }
        // stu
        this.StuForm = this.formBuilder.group({
            fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            ph_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10)]],
            class: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            branch: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            c_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        // hr
        this.HRForm = this.formBuilder.group({
            fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            ph_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10)]],
            class: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            branch: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            designation: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            date_of_joining: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            moto_of_joining: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            c_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        //  Others
        this.OtherForm = this.formBuilder.group({
            fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            ph_no: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(10)]],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            c_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    // get f() { return this.HRForm.controls; }
    RegisterComponent.prototype.onHrSubmit = function () {
        var _this = this;
        this.hrSubmitted = true;
        console.log(this.HRForm.value);
        if (this.HRForm.invalid) {
            alert('fill all details');
            return;
        }
        else {
            this.usersService.registerHR(this.HRForm.value).subscribe(function (res) {
                alert('Registred successfully');
                _this.router.navigateByUrl('/login');
            }, function (err) {
                if (err.status === 422) {
                    alert('Email/id already exists');
                }
                else {
                    alert('reg Failed');
                }
            });
        }
    };
    // Stu aubmit
    RegisterComponent.prototype.onStuSubmit = function () {
        var _this = this;
        this.StuSubmitted = true;
        console.log(this.HRForm.value);
        if (this.StuForm.invalid) {
            alert('fill all details');
            return;
        }
        else {
            this.usersService.registerStu(this.StuForm.value).subscribe(function (res) {
                alert('Registred successfully');
                _this.router.navigateByUrl('/login');
            }, function (err) {
                if (err.status === 422) {
                    alert('Email/id already exists');
                }
                else {
                    alert('reg Failed');
                }
            });
        }
    };
    // Otehr submit
    RegisterComponent.prototype.onOtherSubmit = function () {
        var _this = this;
        this.OtherSubmitted = true;
        console.log(this.HRForm.value);
        if (this.OtherForm.invalid) {
            alert('fill all details');
            return;
        }
        else {
            this.usersService.registerOther(this.OtherForm.value).subscribe(function (res) {
                alert('Registred successfully');
                _this.router.navigateByUrl('/login');
            }, function (err) {
                if (err.status === 422) {
                    alert('Email/id already exists');
                }
                else {
                    alert('reg Failed');
                }
            });
        }
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/components/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.css":
/*!********************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\n  /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); */\n  max-width: 320px;\n  max-height: 125px;\n  margin: auto;\n  text-align: center;\n  font-family: arial;\n}\n\n.title {\n  color: grey;\n  font-size: 18px;\n}\n\nbutton {\n  border: none;\n  outline: 0;\n  display: inline-block;\n  padding: 8px;\n  color: white;\n  background-color: #000;\n  text-align: center;\n  cursor: pointer;\n  width: 100%;\n  font-size: 18px;\n}\n\na {\n  text-decoration: none;\n  font-size: 22px;\n  color: black;\n}\n\nimg\n{\n  border-radius: 50%;\n  /* border-radius: 50%; */\n  max-height: 250px;\n  max-width: 100%;\n}\n\nbutton:hover, a:hover {\n  opacity: 0.7;\n}"

/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- {{userDetails.fullname}} {{userDetails.email}} {{userDetails.}} -->\n<div>\n    <table>\n        <tr>\n            <td>fullname</td>\n            <td> {{userDetails.fullName}}</td>\n        </tr>\n        <tr>\n            <td>Id:</td>\n            <td> {{userDetails.id}}</td>\n        </tr>\n        <tr>\n            <td>email</td>\n            <td> {{userDetails.email}}</td>\n        </tr>\n        <tr>\n            <td>Ph N0</td>\n            <td> {{userDetails.ph_no}}</td>\n        </tr>\n        <tr>\n            <td>class</td>\n            <td> {{userDetails.class}}</td>\n        </tr>\n        <tr>\n            <td>branch</td>\n            <td> {{userDetails.branch}}</td>\n        </tr>\n        <tr>\n            <td>Designation</td>\n            <td> {{userDetails.designation}}</td>\n        </tr>\n        <tr>\n            <td>Date_of_joining</td>\n\n            <td></td>\n            <td> {{userDetails.date_of_joining}}</td>\n        </tr>\n    </table>\n</div>\n\n\n<!-- <input type=\"button\" (click)=\"onLogout()\" value=\"Logout\" /> -->\n\n<!-- <h2 style=\"text-align:center\">User Profile Card</h2> -->\n<br><br>\n<div class=\"card\">\n  <img src=\"../../../assets/images/hho_logo.jpeg\" alt=\"John\" >\n  <h1>John Doe</h1>\n  <p class=\"title\">CEO & Founder, Example</p>\n  <p>Harvard University</p>\n  <div style=\"margin: 24px 0;\">\n    <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a> &nbsp;&nbsp;&nbsp;\n    <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>  &nbsp;&nbsp;&nbsp;\n    <a href=\"#\"><i class=\"fa fa-linkedin\"></i></a> &nbsp;&nbsp;&nbsp; \n    <a href=\"#\"><i class=\"fa fa-facebook\"></i></a> &nbsp;&nbsp;&nbsp;\n  </div>\n  <p><button>Contact</button></p>\n</div>"

/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.ts ***!
  \*******************************************************************/
/*! exports provided: UserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return UserProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../users.service */ "./src/app/users.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(router, usersService) {
        this.router = router;
        this.usersService = usersService;
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.usersService.getUserProfile().subscribe(function (res) {
            _this.userDetails = res['rep'];
        }, function (err) {
        });
    };
    UserProfileComponent.prototype.onLogout = function () {
        this.usersService.deleteToken();
        this.router.navigate(['/login']);
    };
    UserProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-profile',
            template: __webpack_require__(/*! ./user-profile.component.html */ "./src/app/components/user-profile/user-profile.component.html"),
            styles: [__webpack_require__(/*! ./user-profile.component.css */ "./src/app/components/user-profile/user-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"]])
    ], UserProfileComponent);
    return UserProfileComponent;
}());



/***/ }),

/***/ "./src/app/contactus/contactus.component.css":
/*!***************************************************!*\
  !*** ./src/app/contactus/contactus.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/contactus/contactus.component.html":
/*!****************************************************!*\
  !*** ./src/app/contactus/contactus.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-contact100\">\n    <div class=\"wrap-contact100\">\n        <form class=\"contact100-form validate-form\">\n            <span class=\"contact100-form-title\">\n\t\t\t\t\tSend Us A Message\n\t\t\t\t</span>\n\n            <label class=\"label-input100\" for=\"first-name\">Tell us your name *</label>\n            <div class=\"wrap-input100 rs1-wrap-input100   validate-input\" data-validate=\"Type first name\">\n                <input id=\"first-name\" class=\"input100\" type=\"text\" name=\"first-name\" placeholder=\"First name\">\n                <span class=\"focus-input100\"></span>\n            </div>\n            <div class=\"wrap-input100 rs2-wrap-input100 validate-input\" data-validate=\"Type last name\">\n                <input class=\"input100\" type=\"text\" name=\"last-name\" placeholder=\"Last name\">\n                <span class=\"focus-input100\"></span>\n            </div>\n\n            <label class=\"label-input100\" for=\"email\">Enter your email *</label>\n            <div class=\"wrap-input100 validate-input\" data-validate=\"Valid email is required: ex@abc.xyz\">\n                <input id=\"email\" class=\"input100\" type=\"text\" name=\"email\" placeholder=\"Eg. example@email.com\">\n                <span class=\"focus-input100\"></span>\n            </div>\n\n            <label class=\"label-input100\" for=\"phone\">Enter phone number</label>\n            <div class=\"wrap-input100\">\n                <input id=\"phone\" class=\"input100\" type=\"text\" name=\"phone\" placeholder=\"Eg. +1 800 000000\">\n                <span class=\"focus-input100\"></span>\n            </div>\n\n            <label class=\"label-input100\" for=\"message\">Message *</label>\n            <div class=\"wrap-input100 validate-input\" data-validate=\"Message is required\">\n                <textarea id=\"message\" class=\"input100\" name=\"message\" placeholder=\"Write us a message\"></textarea>\n                <span class=\"focus-input100\"></span>\n            </div>\n\n            <div class=\"container-contact100-form-btn\">\n                <button class=\"contact100-form-btn\">\n\t\t\t\t\t\tSend Message\n\t\t\t\t\t</button>\n            </div>\n        </form>\n\n        <div class=\"contact100-more flex-col-c-m\" style=\"background-image: url('images/bg-01.jpg');\">\n            <div class=\"text-center\">\n                <img src=\"../../app/../assets/hho_logo.jpeg\" class=\"rounded mx-auto d-block hoverable\" alt=\"hho_logo\" height=\"128px\" width=\"128px\">\n            </div>\n            <br><br>\n            <div class=\"flex-w size1 p-b-47\">\n                <br><br>\n                <div class=\"txt1 p-r-25\">\n                    <span class=\"lnr lnr-map-marker\"></span>\n                </div>\n\n                <div class=\"flex-col size2\">\n                    <span class=\"txt1 p-b-20\">\n\t\t\t\t\t\t\tAddress\n\t\t\t\t\t\t</span>\n\n                    <span class=\"txt2\">\n               G-10,AB-1, IIIT R.K.Valley, RGUKT-AP, Vempalli Mdl., Kadapa Dst., Andhra Pradesh - 516330 India\n              </span>\n                </div>\n            </div>\n\n            <div class=\"dis-flex size1 p-b-47\">\n                <div class=\"txt1 p-r-25\">\n                    <span class=\"lnr lnr-phone-handset\"></span>\n                </div>\n\n                <div class=\"flex-col size2\">\n                    <span class=\"txt1 p-b-20\">\n\t\t\t\t\t\t\tLets Talk\n\t\t\t\t\t\t</span>\n\n                    <span class=\"txt3\">\n\t\t\t\t\t\t\t+91 8367778043\n\t\t\t\t\t\t</span>\n                </div>\n            </div>\n\n            <div class=\"dis-flex size1 p-b-47\">\n                <div class=\"txt1 p-r-25\">\n                    <span class=\"lnr lnr-envelope\"></span>\n                </div>\n\n                <div class=\"flex-col size2\">\n                    <span class=\"txt1 p-b-20\">\n\t\t\t\t\t\t\tGeneral Support\n\t\t\t\t\t\t</span>\n\n                    <span class=\"txt3\">\n                        <a href=\"mailto:hhorkv@gmail.com?Subject=Support HHO%20\" target=\"_top\">hhorkv@gmail.com</a>\n                        <br>\n                        <a href=\"mailto:hho@rguktrkv.ac.in?Subject=Support HHO%20\" target=\"_top\">hho@rguktrkv.ac.in</a>\n\t\t\t\t\t\t      </span>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n\n<div id=\"dropDownSelect1\"></div>"

/***/ }),

/***/ "./src/app/contactus/contactus.component.ts":
/*!**************************************************!*\
  !*** ./src/app/contactus/contactus.component.ts ***!
  \**************************************************/
/*! exports provided: ContactusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusComponent", function() { return ContactusComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactusComponent = /** @class */ (function () {
    function ContactusComponent() {
    }
    ContactusComponent.prototype.ngOnInit = function () {
    };
    ContactusComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contactus',
            template: __webpack_require__(/*! ./contactus.component.html */ "./src/app/contactus/contactus.component.html"),
            styles: [__webpack_require__(/*! ./contactus.component.css */ "./src/app/contactus/contactus.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ContactusComponent);
    return ContactusComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-fill-remaining-space {\n    /* This fills the remaining space, by using flexbox. \n       Every toolbar row uses a flexbox row layout. */\n    flex: 1 1 auto;\n}\n\n.example-container {\n    display: flex;\n    flex-direction: column;\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n}\n\n.example-is-mobile .example-toolbar {\n    position: fixed;\n    /* Make sure the toolbar will stay on top of the content as it scrolls past. */\n    z-index: 2;\n}\n\nh1.example-app-name {\n    margin-left: 8px;\n}\n\n.example-sidenav-container {\n    /* When the sidenav is not fixed, stretch the sidenav container to fill the available space. This\n       causes `<mat-sidenav-content>` to act as our scrolling element for desktop layouts. */\n    flex: 1;\n}\n\n.example-is-mobile .example-sidenav-container {\n    /* When the sidenav is fixed, don't constrain the height of the sidenav container. This allows the\n       `<body>` to be our scrolling element for mobile layouts. */\n    flex: 1 0 auto;\n}\n\n.btn {\n    padding-top: 10px;\n    width: 200px;\n    height:70px;\n}\n\n/* signin */\n\n/* .dropbtn {\n    background-color: #4CAF50;\n    color: white;\n    padding: 16px;\n    font-size: 16px;\n    border: none;\n    cursor: pointer;\n} */\n\n/* .dropdown {\n    position: relative;\n    padding-left: 10px;\n    display: inline-block;\n}\n\n.dropdown-content {\n    display: none;\n    position: absolute;\n    background-color: #f9f9f9;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);\n    z-index: 1;\n} */\n\n/* .dropdown-content a {\n    color: black;\n    padding: 12px 16px;\n    text-decoration: none;\n    display: block;\n}\n\n.dropdown-content a:hover {\n    background-color: #f1f1f1\n    \n}\n\n.dropdown:hover .dropdown-content {\n    display: block;\n}\n\n.dropdown:hover .dropbtn {\n    background-color: #3e8e41;\n} */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark indigo \">\n    <a class=\"navbar-brand\" href=\"#\">\n        <!-- <i class=\"fa fa-bars\" aria-hidden=\"\"></i> &nbsp; -->\n        <img src=\"../../app/../assets/hho_logo.jpeg\" class=\"rounded float-left\" height=\"30\" class=\"d-inline-block align-top\" alt=\"hho logo\"> &nbsp;\n\n    </a>\n    <a class=\"navbar-brand\" href=\"#\">Helping Hands Organization</a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarText\" aria-controls=\"navbarText\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n          <span class=\"navbar-toggler-icon\"></span>\n        </button>\n    <div class=\"collapse navbar-collapse\" id=\"navbarText\">\n        <ul class=\"navbar-nav mr-auto\">\n            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n            <li class=\"nav-item active\">\n                <a class=\"nav-link\" href=\"#\">Home\n                <span class=\"sr-only\">(current)</span>\n              </a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/aboutus\">About HHO</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/contactus\">Contact Us</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link \" routerLink=\"/achievements\">Achievements</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/team\">Team</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" routerLink=\"/post\">POST</a>\n            </li>\n        </ul>\n\n    </div>\n    <a *ngIf=\"!usersService.loggedIn()\" class=\"nav-link\" routerLink=\"/login\">Sign In</a>\n    <a *ngIf=\"!usersService.loggedIn()\" class=\"nav-link\" routerLink=\"/register\">Sign Up</a>\n    <!-- <a *ngIf=\"usersService.loggedIn()\" class=\"nav-link\" routerLink=\"/register\">LogOut</a> -->\n    <input *ngIf=\"usersService.loggedIn()\" type=\"button\" (click)=\"onLogout()\" value=\"Logout\" />\n\n</nav>"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// let login: Boolean;
// if ( this.usersService.isLoggedIn()) {
//   login = true;
// } else {
//   login = false;
// }
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(usersService, router) {
        this.usersService = usersService;
        this.router = router;
        this.events = [];
        this.foods = [
            { value: 'steak-0', viewValue: 'HR' },
            { value: 'pizza-1', viewValue: 'non-HR' },
            { value: 'tacos-2', viewValue: 'Victim' }
        ];
    }
    HeaderComponent.prototype.onLogout = function () {
        this.usersService.deleteToken();
        this.router.navigate(['/login']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .carousel-inner img {\n    width: 100%;\n    height: 10%;\n} */\n\n\n/* .gallery {\n    width: 10%;\n    height: 10%;\n} */\n\n\n.image_slide {\n    width: 100%;\n    height: 500px;\n}"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"gallery\">\n    <div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\n        <ul class=\"carousel-indicators\">\n            <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"2\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"3\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"4\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"5\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"6\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"7\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"8\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"9\"></li>\n            <li data-target=\"#demo\" data-slide-to=\"10\"></li>\n        </ul>\n        <div class=\"carousel-inner\">\n            <div class=\"carousel-item active\">\n                <img src=\"../../assets/slider/1.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/2.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/3.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/4.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/5.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/6.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/7.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/8.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/9.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/10.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n            <div class=\"carousel-item\">\n                <img src=\"../../assets/slider/11.jpg\" alt=\"HHO Rkv\" class=\"image_slide\">\n                <div class=\"carousel-caption\">\n                    <h3>HHO</h3>\n                    <p></p>\n                </div>\n            </div>\n        </div>\n        <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n            <span class=\"carousel-control-prev-icon\"></span>\n        </a>\n        <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n            <span class=\"carousel-control-next-icon\"></span>\n        </a>\n    </div>\n</div>\n<br>\n<p class=\"text-center\">\n    <button mat-raised-button color=\"warn\">Full Website will be updated soon...</button>\n</p>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/main-nav/main-nav.component.css":
/*!*************************************************!*\
  !*** ./src/app/main-nav/main-nav.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n    height: 100%;\n}\n\n.sidenav {\n    width: 200px;\n}\n\n/* .hidden {\n    display: none;\n} */\n\n@media(min-width: 768px) {\n    .sidenav {\n        display: none;\n    }\n}\n\n.sidenav .mat-toolbar {\n    background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n    position: -webkit-sticky;\n    position: sticky;\n    top: 0;\n    z-index: 1;\n}\n\n.spacer {\n    flex: 1 1 auto;\n}\n\n.mat-toolbar a {\n    display: inline;\n    margin: 0 10px;\n    color: white;\n    text-decoration: none;\n}"

/***/ }),

/***/ "./src/app/main-nav/main-nav.component.html":
/*!**************************************************!*\
  !*** ./src/app/main-nav/main-nav.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n    <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"false\" [attr.role]=\"!(isHandset$ | async) ? 'dialog' : 'navigation'\" [mode]=\"(isHandset$ | async) ? 'over' : 'side'\" [opened]=\"!(isHandset$ | async)\">\n        <mat-toolbar>Menu</mat-toolbar>\n        <mat-nav-list>\n            <a mat-list-item href=\"#\">Link 1</a>\n            <a mat-list-item href=\"#\">Link 2</a>\n            <a mat-list-item href=\"#\">Link 3</a>\n        </mat-nav-list>\n    </mat-sidenav>\n    <mat-sidenav-content>\n        <mat-toolbar color=\"primary\">\n            <button type=\"button\" aria-label=\"Toggle sidenav\" mat-icon-button (click)=\"drawer.toggle()\" *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n            <span>Helping Hands Organization</span>\n            <span class=\"spacer\"></span>\n            <a mat-list-item href=\"#\">Link 1</a>\n            <a mat-list-item href=\"#\">Link 2</a>\n            <a mat-list-item href=\"#\">Link 3</a>\n\n        </mat-toolbar>\n        <!-- Add Content Here -->\n        <ng-content></ng-content>\n    </mat-sidenav-content>\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/main-nav/main-nav.component.ts":
/*!************************************************!*\
  !*** ./src/app/main-nav/main-nav.component.ts ***!
  \************************************************/
/*! exports provided: MainNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainNavComponent", function() { return MainNavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MainNavComponent = /** @class */ (function () {
    function MainNavComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) { return result.matches; }));
    }
    MainNavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-main-nav',
            template: __webpack_require__(/*! ./main-nav.component.html */ "./src/app/main-nav/main-nav.component.html"),
            styles: [__webpack_require__(/*! ./main-nav.component.css */ "./src/app/main-nav/main-nav.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["BreakpointObserver"]])
    ], MainNavComponent);
    return MainNavComponent;
}());



/***/ }),

/***/ "./src/app/material.ts":
/*!*****************************!*\
  !*** ./src/app/material.ts ***!
  \*****************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/esm5/button-toggle.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















// import {MatDatepickerModule} from '@angular/material/datepicker';
// import {FormControl, Validators} from '@angular/forms';

// import { MatMomentDateModule } from '@angular/material-moment-adapter';




var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatButtonModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_6__["MatListModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_7__["MatChipsModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_8__["MatRippleModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_12__["MatExpansionModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__["MatAutocompleteModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_16__["MatInputModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_17__["MatTabsModule"], _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_19__["MatSlideToggleModule"], _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_20__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTreeModule"]],
            exports: [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatButtonModule"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"], _angular_material_menu__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_6__["MatListModule"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_7__["MatChipsModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_8__["MatRippleModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_12__["MatExpansionModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__["MatAutocompleteModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_16__["MatInputModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_17__["MatTabsModule"], _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_13__["MatDatepickerModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_19__["MatSlideToggleModule"], _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_20__["MatButtonToggleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTreeModule"]],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/my-nav/my-nav.component.css":
/*!*********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n"

/***/ }),

/***/ "./src/app/my-nav/my-nav.component.html":
/*!**********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav #drawer class=\"sidenav\" fixedInViewport=\"true\"\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n      [opened]=\"!(isHandset$ | async)\">\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n      <a mat-list-item href=\"#\">Link 1</a>\n      <a mat-list-item href=\"#\">Link 2</a>\n      <a mat-list-item href=\"#\">Link 3</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\"\n        *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n      <span>hho</span>\n    </mat-toolbar>\n    <!-- Add Content Here -->\n  </mat-sidenav-content>\n</mat-sidenav-container>\n"

/***/ }),

/***/ "./src/app/my-nav/my-nav.component.ts":
/*!********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.ts ***!
  \********************************************/
/*! exports provided: MyNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNavComponent", function() { return MyNavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MyNavComponent = /** @class */ (function () {
    function MyNavComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) { return result.matches; }));
    }
    MyNavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-my-nav',
            template: __webpack_require__(/*! ./my-nav.component.html */ "./src/app/my-nav/my-nav.component.html"),
            styles: [__webpack_require__(/*! ./my-nav.component.css */ "./src/app/my-nav/my-nav.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["BreakpointObserver"]])
    ], MyNavComponent);
    return MyNavComponent;
}());



/***/ }),

/***/ "./src/app/team/team.component.css":
/*!*****************************************!*\
  !*** ./src/app/team/team.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\n    /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); */\n    max-width: 200px;\n    max-height: 300px;\n    margin:auto;\n    text-align: center;\n    font-family: arial;\n  }\n  \n  .title {\n    color: grey;\n    font-size: 12px;\n  }\n  \n  button {\n    border: none;\n    outline: 0;\n    display: inline-block;\n    padding: 8px;\n    color: white;\n    background-color: #000;\n    text-align: center;\n    cursor: pointer;\n    width: 100%;\n    font-size: 18px;\n  }\n  \n  a {\n    text-decoration: none;\n    font-size: 22px;\n    color: black;\n  }\n  \n  img\n  {\n    border-radius: 40%;\n    /* border-radius: 50%; */\n    max-height: 150px;\n    \n    /* max-width: 100%; */\n  }\n  \n  button:hover, a:hover {\n    opacity: 0.7;\n  }\n  \n  .team\n  {\n    max-height: 100%;\n  }"

/***/ }),

/***/ "./src/app/team/team.component.html":
/*!******************************************!*\
  !*** ./src/app/team/team.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2 style=\"text-align:center\">Our Team</h2>\n<div class=\"tot\">\n    <div class=\"row\">\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n        <div class=\"card\">\n            <img src=\"../../assets/hho_logo.jpeg\" class=\"img-circle\" alt=\"John\" style=\"width:100%\">\n\n            <p><button>ASDFGHJKL</button></p>\n            <p class=\"title\">abcdrfghi@gmail.com<br>9876543210</p>\n            <h6>Representative</h6>\n        </div>\n\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/team/team.component.ts":
/*!****************************************!*\
  !*** ./src/app/team/team.component.ts ***!
  \****************************************/
/*! exports provided: TeamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamComponent", function() { return TeamComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TeamComponent = /** @class */ (function () {
    function TeamComponent() {
        this.panelOpenState = false;
    }
    TeamComponent.prototype.ngOnInit = function () {
    };
    TeamComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! ./team.component.html */ "./src/app/team/team.component.html"),
            styles: [__webpack_require__(/*! ./team.component.css */ "./src/app/team/team.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TeamComponent);
    return TeamComponent;
}());



/***/ }),

/***/ "./src/app/users.service.ts":
/*!**********************************!*\
  !*** ./src/app/users.service.ts ***!
  \**********************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-jwt */ "./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_jwt__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersService = /** @class */ (function () {
    // selectedHR: HR = {
    //   fullname: '',
    // email: '',
    // id: '',
    // ph_no: '',
    // class: '',
    // branch: '',
    // moto_of_joining: '',
    // date_of_joining: '',
    // designatio: '',
    // password: '',
    // c_password: ''
    // };
    // selectedStudent: Student = {
    //   fullname: '',
    // email: '',
    // id: '',
    // ph_no: '',
    // class: '',
    // branch: '',
    // password: '',
    // c_password: ''
    // };
    // selectedUser: User = {
    //   fullname: '',
    // email: '',
    // ph_no: '',
    // password: '',
    // c_password: ''
    // };
    function UsersService(http) {
        this.http = http;
        this.noAuthHeader = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'NoAuth': 'True' }) };
    }
    UsersService.prototype.registerHR = function (hr) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/register', hr, this.noAuthHeader);
    };
    UsersService.prototype.registerStu = function (Stu) {
        // alert('called');
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/stu_register', Stu, this.noAuthHeader);
    };
    UsersService.prototype.registerOther = function (Other) {
        // alert('called');
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/others_register', Other, this.noAuthHeader);
    };
    UsersService.prototype.login = function (authCredentials) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/authenticate', authCredentials, this.noAuthHeader);
    };
    UsersService.prototype.getUserProfile = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/userProfile');
    };
    UsersService.prototype.achievepost = function (post) {
        // alert('called');
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/achievementpost', post, this.noAuthHeader);
    };
    UsersService.prototype.achievements = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/achievements', this.noAuthHeader);
    };
    UsersService.prototype.setToken = function (token) {
        localStorage.setItem('token', token);
    };
    UsersService.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    // for hide buttons
    UsersService.prototype.loggedIn = function () {
        return Object(angular2_jwt__WEBPACK_IMPORTED_MODULE_3__["tokenNotExpired"])();
    };
    UsersService.prototype.deleteToken = function () {
        localStorage.removeItem('token');
    };
    UsersService.prototype.getUserPayload = function () {
        var token = this.getToken();
        if (token) {
            var userPayload = atob(token.split('.')[1]);
            return JSON.parse(userPayload);
        }
        else {
            return null;
        }
    };
    UsersService.prototype.isLoggedIn = function () {
        var userPayload = this.getUserPayload();
        if (userPayload)
            return userPayload.exp > Date.now() / 1000;
        else
            return false;
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiBaseUrl: 'https://limitless-atoll-35343.herokuapp.com/api'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);




// material design

if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/jyoshna/Desktop/dec/hho/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map