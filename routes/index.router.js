const express = require('express');
const router = express.Router();

const ctrlRep = require('../controllers/rep.controller');

const ctrlachieve = require('../controllers/achieve.controller');

const ctrlproblempost = require('../controllers/problem_post.controller');

const ctrlAccinfopost = require('../controllers/accinfo.controller');

const jwtHelper = require('../config/jwtHelper');

const ctrlSpecial = require('../controllers/today_special');

const ctrlContactus = require('../controllers/contactus.controller');



router.post('/register', ctrlRep.register);
router.post('/stu_register', ctrlRep.stu_register);
router.post('/others_register', ctrlRep.others_register);
router.post('/authenticate', ctrlRep.authenticate);
router.get('/userProfile', jwtHelper.verifyJwtToken, ctrlRep.userProfile);
router.post('/achievementpost', ctrlachieve.achievementpost);
router.get('/achievements', ctrlachieve.achievements);
router.post('/send_mail', jwtHelper.verifyJwtToken, ctrlRep.send_mail);
router.get('/otp', jwtHelper.verifyJwtToken, ctrlRep.otp);
router.post('/problem_post', jwtHelper.verifyJwtToken, ctrlproblempost.problem_post);
router.get('/problems_list', jwtHelper.verifyJwtToken, ctrlproblempost.problems_list);
router.post('/problem_status_update/:id', jwtHelper.verifyJwtToken, ctrlproblempost.problem_status_update);
router.post('/accinfo_post', jwtHelper.verifyJwtToken, ctrlAccinfopost.accinfo_post);
router.get('/accinfo', ctrlAccinfopost.accinfo);
router.put('/hr_verify/:id', jwtHelper.verifyJwtToken, ctrlRep.hr_verify);
router.get('/hrs_list', ctrlRep.hrs_list);
router.get('/admin_verify', jwtHelper.verifyJwtToken, ctrlRep.admin_verify);
router.post('/TodaySpecial_post', ctrlSpecial.TodaySpecial_post);
router.get('/TodaySpecial', ctrlSpecial.TodaySpecial);
router.post('/accbalance_post', jwtHelper.verifyJwtToken, ctrlAccinfopost.accbalance_post);
router.post('/accbalance_update', jwtHelper.verifyJwtToken, ctrlAccinfopost.AccBalance_update);
router.get('/accbalance_get', ctrlAccinfopost.AccBalance_get);
router.post('/accsheet_post', jwtHelper.verifyJwtToken, ctrlAccinfopost.Accsheet_post);
router.get('/accsheet_get', ctrlAccinfopost.Accsheet_get);
router.post('/contactus', ctrlContactus.contactus);



module.exports = router;