const mongoose = require('mongoose');
const _ = require('lodash');

const Accinfo_post = mongoose.model('Acc_Info');
const AccBalance = mongoose.model('AccBalance');
const AccSheet = mongoose.model('AccSheet');
mongoose.set('useFindAndModify', false);


module.exports.accinfo_post = (req, res, next) => {
    var accinfo_post = new Accinfo_post();

    console.log(req._id);
    accinfo_post.post_by = req._id;
    accinfo_post.date = req.body.date;
    accinfo_post.batch = req.body.batch;
    accinfo_post.branch = req.body.branch;
    accinfo_post.class = req.body.class;
    accinfo_post.purpose = req.body.purpose;
    accinfo_post.collected_amount = req.body.collected_amount;



    accinfo_post.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
            return next(err);
    });

}


module.exports.accinfo = (req, res, next) => {
    Accinfo_post.find({},
        (err, accinfo) => {
            if (!accinfo)
                return res.json(err);
            else
                return res.status(200).json({ status: true, accinfo: accinfo });
        })
}




//update toatal money
module.exports.AccBalance_update = (req, res, next) => {
    AccBalance.findOneAndUpdate({ _id: "5c77b410a220ae42ee397459" }, { $set: { Total_Money: req.body.money } }, function(err, doc) {
        if (err) {
            res.send(err);
        } else {
            console.log('success');
            return res.status(202).json({ status: false, message: 'updated successfully' });

        }
    });
}

//get total money

module.exports.AccBalance_get = (req, res, next) => {
    AccBalance.find({},
        (err, accbalance) => {
            if (!accbalance)
                return res.json(err);
            else
                return res.status(200).json({ status: true, accbalance: accbalance });
            // return res.status(200).json({ status: true, accbalance: _.pick(accbalance, ['Total_Money']) });
            // return res.status(200).json({ status: true, rep: _.pick(rep, ['fullName', 'email', 'id', 'ph_no', 'class', 'branch', 'designation', 'date_of_joining', 'otp']) });


        })
}

//

// get accsheet
module.exports.Accsheet_get = (req, res, next) => {
    AccSheet.find({},
        (err, sheet) => {
            if (!sheet)
                return res.json(err);
            else
                return res.status(200).json({ status: true, accsheet: sheet });
            // return res.status(200).json({ status: true, accbalance: _.pick(accbalance, ['Total_Money']) });
            // return res.status(200).json({ status: true, rep: _.pick(rep, ['fullName', 'email', 'id', 'ph_no', 'class', 'branch', 'designation', 'date_of_joining', 'otp']) });


        })
}

//update accsheet
module.exports.Accsheet_post = (req, res, next) => {
    // money_in_acc;
    var accsheet_post = new AccSheet();

    console.log(req.body.amount);
    accsheet_post.update_by = req._id;
    accsheet_post.amount = req.body.amount;
    accsheet_post.purpose = req.body.purpose;
    var money_in_acc = "scope";
    var money_in_acc1, money;


    if (!(req._id == "5c70468093502d39a7755518")) {
        console.log('admin not  verified');
        return res.json(error);
    } else {
        AccBalance.find({},
            (err, accbalance) => {
                if (!accbalance)
                    return res.json(err);
                else {

                    //  res.status(200).json({ status: true, rep: _.pick(rep, ['fullName', 'email', 'id', 'ph_no', 'class', 'branch', 'designation', 'date_of_joining', 'otp']) });
                    // console.log('main ' + accbalance);
                    console.log('string' + accbalance);
                    money_in_acc = _.pick(accbalance, ['Total_Money']);
                    var string = JSON.stringify(accbalance);
                    var jsonObj = JSON.parse(string);
                    this.money_in_acc1 = jsonObj[0]['Total_Money'];
                    console.log('string ' + jsonObj[0]['Total_Money']);
                    // console.log('string' + string['Total_Money']);
                    money = jsonObj[0]['Total_Money'] + req.body.amount;
                    console.log(req.body.amount);

                    // updating accbalance
                    AccBalance.findOneAndUpdate({ _id: "5c77b410a220ae42ee397459" }, { $set: { Total_Money: (jsonObj[0]['Total_Money'] + parseInt(req.body.amount)) } }, function(err, doc) {
                        if (err) {
                            // console.log('error');
                            res.send(err);
                        } else {
                            // console.log('success');

                            // get balance for total amount

                            AccBalance.find({},
                                (err, accbalance) => {
                                    if (!accbalance)
                                        return res.json(err);
                                    else {
                                        // console.log('in last');
                                        // console.log('in last' + accbalance.Total_Money);
                                        var string = JSON.stringify(accbalance);
                                        var jsonObj = JSON.parse(string);

                                        accsheet_post.total_amount = jsonObj[0]['Total_Money'];
                                        console.log(accsheet_post.total_amount);
                                        accsheet_post.save((err, doc) => {
                                            if (!err)
                                                res.send(doc);
                                            else
                                                return next(err);
                                        });
                                    }


                                })

                            // end get balance for total amount

                        }
                    });


                }
                // return res.status(200).json({ status: true, accbalance: _.pick(accbalance, ['Total_Money']) });


            });


    }
    // accsheet_post.total_amount = 90;


    // getting main acc money





}






//post acc balance
module.exports.accbalance_post = (req, res, next) => {
    var accbalance = new AccBalance();

    if (!(req._id == "5c70468093502d39a7755518")) {
        console.log('admin not  verified');
        return res.json(error);
    } else {
        accbalance.Total_Money = req.body.money;


        accbalance.save((err, doc) => {
            if (!err)
                res.send(doc);
            else
                return next(err);
        });
    }


}