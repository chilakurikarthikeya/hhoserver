const mongoose = require('mongoose');
const _ = require('lodash');

// const Achieve = mongoose.model('TodaySpecial')
const Today_special = mongoose.model('TodaySpecial');
mongoose.set('useFindAndModify', false);

module.exports.TodaySpecial_post = (req, res, next) => {
    console.log('in special');
    var today_special = new Today_special();

    today_special.name = req.body.name;
    today_special.date = req.body.date;
    today_special.description = req.body.description;


    today_special.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
            return next(err);
    });

}

module.exports.TodaySpecial = (req, res, next) => {
    Today_special.find({},
        (err, special) => {
            if (!special)
                return res.json(err);
            else
                return res.status(200).json({ status: true, special: special });
        })
}


// const sgMail = require('@sendgrid/mail');
// module.exports.send_mail=(req,res,next) =>  {
//     sgMail.setApiKey(process.env.SENDGRID_API_KEY);
// const msg = {
//   to: 'hhorkv@gmail.com',
//   from: 'test@example.com',
//   subject: 'Sending with SendGrid is Fun',
//   text: 'and easy to do anywhere, even with Node.js',
//   html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };
// sgMail.send(msg);

// }