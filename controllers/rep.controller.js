const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');
const Rep = mongoose.model('Rep');
const Student = mongoose.model('Student');
const Others = mongoose.model('Others');

const achieve = mongoose.model('Achievements')
const problem_post = mongoose.model('Posted_Problems');
mongoose.set('useFindAndModify', false);

var randomInt = require('random-int');
var email = '';


// hr register
module.exports.register = (req, res, next) => {

    var rep = new Rep();
    rep.fullName = req.body.fullname;
    rep.id = req.body.id;
    rep.email = req.body.email;
    rep.class = req.body.class;
    rep.branch = req.body.branch;
    rep.ph_no = req.body.ph_no;
    rep.designation = req.body.designation;
    rep.moto_of_joining = req.body.moto_of_joining;
    rep.date_of_joining = req.body.date_of_joining;
    rep.password = req.body.password;
    rep.otp = false;
    rep.admin_verify = false;


    rep.save((err, doc) => {

        if (!err)
            res.send(doc);
        else {
            posted
            if (err.code == 11000)
                res.status(422).send(['Duplicate email adrress found.']);
            else
                return next(err);
        }
    });


}

//  student register
module.exports.stu_register = (req, res, next) => {

    console.log('called');
    var student = new Student();
    student.fullName = req.body.fullname;
    student.id = req.body.id;
    student.email = req.body.email;
    student.class = req.body.class;
    student.branch = req.body.branch;
    student.ph_no = req.body.ph_no;
    student.password = req.body.password;
    student.otp = false;


    student.save((err, doc) => {

        if (!err)
            res.send(doc);
        else {
            if (err.code == 11000)
                res.status(422).send(['Duplicate email adrress found.']);
            else
                return next(err);
        }
    });


}

// others register

module.exports.others_register = (req, res, next) => {

    var others = new Others();
    others.fullName = req.body.fullname;
    others.id = req.body.id;
    others.email = req.body.email;
    others.ph_no = req.body.ph_no;
    others.password = req.body.password;
    others.otp = false;

    others.save((err, doc) => {

        if (!err)
            res.send(doc);
        else {
            if (err.code == 11000)
                res.status(422).send(['Duplicate email adrress found.']);
            else
                return next(err);
        }
    });


}


// hr authenticate
module.exports.authenticate = (req, res, next) => {
    //call fro passport authentication
    passport.authenticate('local', (err, user, info) => {
        //error from passport middleware
        if (err) return res.status(400).json(err);
        // error from passport middleware
        if (err) return res.status(400).json(err);

        // error if not approved by admin

        // registered user
        else if (user) return res.status(200).json({ "token": user.generateJwt() });
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
}

// student quthenticate
module.exports.authenticate_student = (req, res, next) => {
    //call fro passport authentication
    passport.authenticate('local', (err, user, info) => {
        //error from passport middleware
        if (err) return res.status(400).json(err);
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered user
        else if (user) return res.status(200).json({ "token": user.generateJwt() });
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
}


module.exports.userProfile = (req, res, next) => {
    console.log(req._id);
    Rep.findOne({ _id: req._id },
        (err, rep) => {
            if (!rep)
                return res.status(404).json({ status: false, message: 'User record not found.' });
            else
                return res.status(200).json({ status: true, rep: _.pick(rep, ['fullName', 'email', 'id', 'ph_no', 'class', 'branch', 'designation', 'date_of_joining', 'otp']) });
        }
    );
}

// opt verify
module.exports.otp = (req, res, next) => {
    console.log(req._id);
    Rep.findOneAndUpdate({ _id: req._id }, { $set: { otp: true } }, function(err, doc) {
        if (err) {
            res.send(err);
        } else {
            console.log('success');
            return res.status(202).json({ status: false, message: 'OTP status updated' });

        }
    });
}

// start of admin approve for hr

module.exports.hr_verify = (req, res, next) => {
        console.log('in hr_verify')
        console.log(req.params.id);

        if (!(req._id == "5c70468093502d39a7755518")) {
            console.log('admin not  verified');
            return res.json(error);
        } else {
            Rep.findOneAndUpdate({ _id: req.params.id }, { $set: { admin_verify: true } }, function(err, doc) {
                if (err) {
                    res.send(err);
                } else {
                    console.log('success');
                    return res.status(202).json({ status: false, message: 'Apporved successfully' });

                }
            });
        }



    }
    // start of admin approve for hr

// get hr users list
module.exports.hrs_list = (req, res, next) => {
    Rep.find({},
        (err, rep) => {
            if (!rep)
                return res.json(err);
            else
                return res.status(200).json({ status: true, rep: rep });
        })
}

// end get hr users list

// start of admin verify fro admin panal
module.exports.admin_verify = (req, res, next) => {
        // var id=req._id=="5c70468093502d39a7755518";
        console.log(req._id);

        Rep.findOne({ _id: req._id },
            (err, rep) => {
                if (!rep)
                    return res.status(404).json({ status: false, message: 'User record not found.' });
                else {
                    console.log('hr verify' + rep.admin_verify);
                    if (rep.admin_verify) {
                        console.log('hr verify' + rep.admin_verify);
                        return res.status(200).json({ status: false, message: 'User record not found.' });

                    } else {
                        return res.status(404).json({ status: true, message: 'User record  found.' });

                    }
                }
            }
        );

        // if (!(req._id == "5c70468093502d39a7755518")) {
        //     console.log('admin not  verified');
        //     return res.json(error);
        // } else {
        //     console.log('admin   verified');
        //     return res.status(200).json({ status: true, message: 'success' });
        // }

    }
    // end of admin verify admin panall

module.exports.send_mail = (req, res, next) => {
    // var otp = 0;
    // var email = '';
    //getting email
    // console.log('mail');
    otp = req.body.otp1;
    console.log(otp);

    Rep.findOne({ _id: req._id },
        (err, rep) => {
            if (!rep) {
                // return res.status(404).json({ status: false, message: 'User record not found.' });
                console.log('mail not found');
                return res.status(404).json({ status: false, message: 'User record not found.' });

            } else {
                // return res.status(200).json({ status: true, rep: _.pick(rep, ['fullName', 'email', 'id', 'ph_no', 'class', 'branch', 'designation', 'date_of_joining', 'otp']) });
                email = rep['email'];
                otp1 = send_otp(otp);
                return res.status(202).json({ status: false, message: 'OTP sent Successfully' });



            }
        }
    );

    console.log('out of id =' + email);

    // send_mail((res, err) => {


}

function send_otp(otp1) {
    var otp = otp1;
    "use strict";
    const nodemailer = require("nodemailer");

    // async..await is not allowed in global scope, must use a wrapper
    async function main() {

        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        let account = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'chilakurikarthikeya@gmail.com', // generated ethereal user
                pass: 'KARTHIK@keya3' // generated ethereal password
            }
        });

        // otp = randomInt(1000, 9999);
        console.log(otp);
        console.log('in =' + email);
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Fred Foo 👻" hhorkv@gmial.com', // sender address
            to: email, // list of receivers
            subject: "Verification OTP ✔", // Subject line
            text: "OTP", // plain text body
            html: "<b>OTP=</b>" + otp // html body
        };

        // send mail with defined transport object
        let info = await transporter.sendMail(mailOptions)
            // if (info)
            //     return otp;
        console.log("Message sent: %s", info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        // return otp;
    }

    if (main().catch(console.error)) {
        return otp;
        // return res.status(404).json('success');
        // return res.send(otp);
        // return res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500).send(otp);
        // return res.json({ status: true, achieve: otp });
    }


    // res.send("success");
    // return res.send(otp);
    // return res.status(statusCode >= 100 && statusCode < 600 ? err.code : 500).send(otp);
    // return res.json({ status: true, achieve: otp });



    return otp;

    // });
}


function check_hr_approvr(id) {

}
// module.exports.send_otp = (req, res, next) => {


// }