const mongoose = require('mongoose');
const _ = require('lodash');

const Problem_post = mongoose.model('Posted_Problems');
const problem_post = mongoose.model('Posted_Problems');
mongoose.set('useFindAndModify', false);


module.exports.problem_post = (req, res, next) => {
    var problem_post = new Problem_post();

    console.log(req._id);
    problem_post.postby = req._id;
    problem_post.Victim_Name = req.body.Victim_Name;
    problem_post.id_no = req.body.id_no;
    problem_post.email = req.body.email;
    problem_post.ph_no = req.body.ph_no;
    problem_post.branch = req.body.branch;
    problem_post.class = req.body.class;
    problem_post.problem = req.body.problem;
    problem_post.problem_description = req.body.problem_description;
    problem_post.required_amount = req.body.required_amount;
    problem_post.doc_path = req.body.doc_path;
    problem_post.status = 'posted';

    console.log(problem_post.doc_path);

    problem_post.save((err, doc) => {
        if (!err) {
            send_mail();
            res.send(doc);
        } else
            return next(err);
    });

}

// get problems list 

module.exports.problems_list = (req, res, next) => {

    problem_post.find({},
        (err, problems) => {
            if (!problems)
                return res.json(err);
            else
                return res.status(200).json({ status: true, problems: problems })
        }
    )
}

// start update problem status

module.exports.problem_status_update = (req, res, next) => {
        console.log(req.params.id);
        problem_post.findOneAndUpdate({ _id: req.params.id }, { $set: { status: req.body.status } }, function(err, doc) {
            if (err) {
                res.send(err);
            } else {
                console.log('success');
                return res.status(202).json({ status: false, message: 'updated successfully' });

            }
        });
    }
    // end  update problem status

function send_mail() {
    "use strict";
    const nodemailer = require("nodemailer");

    // async..await is not allowed in global scope, must use a wrapper
    async function main() {

        // Generate test SMTP service account from ethereal.email
        // Only needed if you don't have a real mail account for testing
        let account = await nodemailer.createTestAccount();

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'chilakurikarthikeya@gmail.com', // generated ethereal user
                pass: 'KARTHIK@keya3' // generated ethereal password
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Fred Foo 👻" <foo@example.com>', // sender address
            to: "r131043@rguktrkv.ac.in, r151517@rguktrkv.ac.in", // list of receivers
            subject: "New problem posted", // Subject line
            text: "new problem posted", // plain text body
            html: "new problem posted" // html body
        };

        // send mail with defined transport object
        let info = await transporter.sendMail(mailOptions)

        console.log("Message sent: %s", info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    main().catch(console.error);

}